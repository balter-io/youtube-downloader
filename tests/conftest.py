import pytest
from pytubefix import YouTube


@pytest.fixture
def url(url='https://youtu.be/hqTBxTqbInk'):
    return url


@pytest.fixture
def video_fixture(url):
    test_video = YouTube(url)
    return test_video


@pytest.fixture
def audio_fixture(url):
    test_audio = YouTube(url)
    return test_audio

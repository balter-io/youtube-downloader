import requests
from .conftest import video_fixture


def test_video_resolution(video_fixture):  # only want 1080p video streams
    stream = video_fixture.streams.filter(res='1080p').first()

    assert stream.resolution == '1080p'


def test_not_progressive(video_fixture):  # do not want progressive streams
    stream = video_fixture.streams.filter(res='1080p').first()

    assert stream.is_progressive is False


def test_ok_200_response(url):  # check the response was a 200 ok
    response = requests.get(url)

    assert response.status_code == 200


from .conftest import video_fixture


def test_audio_only_stream(video_fixture):  # only want audio only streams
    stream = video_fixture.streams.filter(only_audio=True).order_by('abr').last()

    assert stream.type == 'audio'


def test_not_progressive(video_fixture):  # do not want progressive streams
    stream = video_fixture.streams.filter(only_audio=True).order_by('abr').last()

    assert stream.is_progressive is False

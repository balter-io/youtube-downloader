from pytubefix import YouTube
from pytubefix.cli import on_progress


def get_video(url):
    youtube_video = YouTube(url, use_po_token=True, token_file='../po_token.json', on_progress_callback=on_progress).streams.filter(res='1080p', progressive=False).first().download(filename='video.mp4')
    return youtube_video

from pytubefix import YouTube
from pytubefix.cli import on_progress


def get_audio(url):
    youtube_audio = YouTube(url, use_po_token=True, token_file='../po_token.json', on_progress_callback=on_progress).streams.filter(only_audio=True).order_by('abr').last().download(filename='audio.webm')
    return youtube_audio

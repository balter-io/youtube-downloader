#!/usr/bin/env python3

import os
import sys
from components.get_video import get_video
from components.get_audio import get_audio
from components.merge_components import merge_streams


def main():
    # download video stream
    print(r'Getting video stream....')
    get_video(sys.argv[1])
    print(f'Complete.')

    # download audio stream
    print(f'Getting audio stream....')
    get_audio(sys.argv[1])
    print(f'Complete.')

    # merge video and audio with ffmpeg
    print(f'Merging video and audio....')
    merge_streams()
    print(f'Complete!')

    # remove separate files
    print(f'Removing original separate files')
    original_files = ['audio.webm', 'video.mp4']
    for file in original_files:
        os.remove(file)
    print(f'Complete!')


if __name__ == '__main__':
    main()
